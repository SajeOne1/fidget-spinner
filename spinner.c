//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// Author: Shane Brown
// Description: Spins your life away
// Date: 21/09/17
// Version: 1.0 - Initial
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>
#include <math.h>

void writeScore(int score);
int readScore();


int main(int argc, char* argv[]){

	// Amount of times spun(counter)
	int spinCount = 0;

	// Exponential vars(x=x, waitTime=y)
	int x = 0;
	double waitTime = 0;

	// Current position of spinner
	int spinnerState = 0;

	// Spinner ascii array
	char *spinnerPos[4];
	spinnerPos[0] = "    OOO   OOO\n   OO OO OO OO\n    OOOOOOOOO\n       O0O\n      OOOOO\n      OO OO\n       OOO\n"; spinnerPos[1] = "       OOO   OOO\n     OO OO OO OO\n     OOOOOOOOO\n       O0O\n     OOOOO\n    OO OO\n   OOO\n";
	spinnerPos[2] = "       OOO\n      OO OO\n      OOOOO\n       O0O\n    OOOOOOOOO\n   OO OO OO OO\n    OOO   OOO\n";
	spinnerPos[3] = "          OOO\n        OO OO\n       OOOOO\n       O0O\n   OOOOOOOOO\n OO OO OO OO\n OOO   OOO\n";

	//spinCount = readScore();

	// Setup ncurses
	initscr();
	printw("%s", spinnerPos[0]);
	refresh();

	// Begin spin loop
	while(true){
		printw("\nPress Space to spin. Press q to quit.\n\nNumber of Spins: %d\n", spinCount);
		int input = getch();
		if(input == 32){ // Space bar pressed
			spinCount++;

			// Spin until spin rate is too slow(decelleration)
			while(waitTime <= 3.0){
				waitTime = pow(2, 0.06 * (x - 6));
				usleep(waitTime * 100000);
				clear();

				// Reset spinner when current spin is complete, or iterate pos of spin
				if(spinnerState >= 3){
					spinnerState = 0;
				}else{
					spinnerState++;
				}

				// Display new spinner position to user
				printw("%s", spinnerPos[spinnerState]);
				refresh();
				// Iterate x in exponential function
				x++;
			}
		}else if(input == 113 || input == 81){ // User pressed q (quit)
			//writeScore(spinCount);
			clear();
			printw("Come back again and spin your life away.");
			break;
		}

		// Reset exponential vars after all spins have completed
		x = 0;
		waitTime = 0;
	}

	// Close ncurses
	refresh();
	getch();
	endwin();

	return 0;
}

/* UNUSED SCORING SYSTEM - TARGET v1.1 */

void writeScore(int score){
	FILE *fp;

    fp = fopen("score.txt", "w+"); fprintf(fp, "%d", score);	
	fclose(fp);
}

int readScore(){
	FILE *fp;
	int score;
	char buff[11];
	fp = fopen("score.txt", "r");
	if(access(fp, F_OK) != 1){
		fscanf(fp, "%s", buff);
		fclose(fp);
		
		//int score = atoi(buff);
		score = 3;
		printf("%d", score);
	}else{
		score = 0;
	}

	return score;
}
